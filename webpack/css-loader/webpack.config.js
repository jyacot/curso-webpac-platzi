const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const dirSource = path.resolve(__dirname, '../../src/');
const dirOutput = path.resolve(__dirname, '../../dist/');


module.exports = {
    mode: "development",
    entry: {
        index : dirSource+'/js/index.js'
    },
    output: {
        path: dirOutput,
        filename: "[name].js"
    },
    devServer:{
        port:9000,
        compress:true

    },
    module:{
        rules:[
           {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: "css-loader"
                })
           },
           {
               test: /\.js$/,
               use:{
                loader: 'babel-loader',
                options:{
                    presets: ['es2015']
                }
               }
           }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/[name].css')
    ]
}